db.fruits.insertMany([
            {
                name : "Apple",
                color : "Red",
                stock : 20,
                price: 40,
                supplier_id : 1,
                onSale : true,
                origin: [ "Philippines", "US" ]
            },

            {
                name : "Banana",
                color : "Yellow",
                stock : 15,
                price: 20,
                supplier_id : 2,
                onSale : true,
                origin: [ "Philippines", "Ecuador" ]
            },

            {
                name : "Kiwi",
                color : "Green",
                stock : 25,
                price: 50,
                supplier_id : 1,
                onSale : true,
                origin: [ "US", "China" ]
            },

            {
                name : "Mango",
                color : "Yellow",
                stock : 10,
                price: 120,
                supplier_id : 2,
                onSale : false,
                origin: [ "Philippines", "India" ]
            }
        ]);

// MongoDB Aggregation

	// Using the aggregate method:
	
	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}
		}}

	]);

	// Field projection with aggregation

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}
		}},
		{$project: {_id: 0}}

	]);

	// Sorting Aggregated results

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total:{ $sum: "$stock"} }},
		{$sort: {total: 1}}

	]);

	// Aggregating results based on array field

	db.fruits.aggregate([

		{$unwind: "$origin"},
		{$group: {_id: "$origin", kinds: {$sum: 1}}}
	]);


	// Other Aggregate stages

	// $count
	db.fruits.aggregate([
			{$match: {color: "Yellow"}},
			{$count: "Yellow Fruits"}
	]);

	// $avg - gets the ave value of stock
	db.fruits.aggregate([
			{$match: {color: "Yellow"}},
			{$group:{_id: "$color", yellow_fruits_stock:{$avg: "$stock"}}}
	]);

	// $min & $max
	db.fruits.aggregate([
			{$match: {color: "Yellow"}},
			{$group:{_id: "$color", yellow_fruits_stock:{$max: "$stock"}}}
	]);